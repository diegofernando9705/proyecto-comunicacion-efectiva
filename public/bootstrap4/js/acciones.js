$(document).ready(function () {
    
    $(document).ready(function () {
        $('.mi-selector').select2();
        $('.mi-selector-tabla').select2();
        $('.mi-selector-tabla-producto').select2();

    });

	$(document).on("click",".btnVer",function(){
		
		$('#ver_modales').modal({
            show: true
        });

		href = $(this).attr("data-url");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });
	});

    $(document).on("click",".btnEdicion",function(){
        
        $('#ver_modales').modal({
            show: true
        });

        href = $(this).attr("data-url");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_categoria", function(){
        var data = $(this).val();
        $.ajax({
            url: '/categorias/conferencias/keyup/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_regional", function(){
        var data = $(this).val();
        $.ajax({
            url: '/regionales/keyup/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_conductores", function(){
        var data = $(this).val();
        $.ajax({
            url: '/remisiones/keyup/conductores/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    /* funcion busqueda cliente */
    $(document).on("change",".mi-selector",function(){
        

        if($(this).attr('data-informacion')){
            
            var identificacion = $(this).val();
            var atributo = $(this).attr('data-informacion');

            $.ajax({
                url: '/remisiones/onchange/conductor/' + atributo + '/' + identificacion,
                method: 'get',
                success: function(data){
                    console.log(data);
                    $("#placa_conductor").val(data[0]['placa_conductor']).trigger("change.select2");
                    $("#identificacion_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");
                    $("#nombre_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");
                    $("#celular_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");

                    
                }
            });

        }else{

            var identificacion = $(this).val();
        
            $.ajax({
                url: '/remisiones/onchange/cliente/' + identificacion,
                method: 'get',
                success: function(data){
                    $("#identificacion_formulario").val(data[0]['identificacion']);
                    $("#direccion_formulario").val(data[0]['direccion_cliente']);
                    $("#ciudad_formulario").val(data[0]['ciudad_cliente']);
                    $("#telefono_formulario").val(data[0]['telefono_fijo_cliente']+" - "+data[0]['telefono_fijo_cliente']+" - "+data[0]['celular_1_cliente']+" - "+data[0]['celular_2_cliente']);
                    $("#email_formulario").val(data[0]['email_cliente']);

                    console.log(data[0]['identificacion']);
                }
            });

        }
    });

    /* funcion busqueda cliente */
    $(document).on("change",".mi-selector-tabla",function(){
        
        var valor = $(this).val();
        var ide = $(this).attr('data-id');
        var id = "#"+ide;

        $.ajax({
            url: '/remisiones/onchange/producto_tabla/' + valor,
            method: 'get',
            success: function(data){
                $(id).val(valor).trigger("change.select2");
                console.log(data[0]['descripcion_producto']);
            }
        });
    });

    $(document).on("change",".mi-selector-tabla-producto",function(){
            
        var valor = $(this).val();
        var ide = $(this).attr('data-id');
        var id_co = "#codigo_"+ide;

        $.ajax({
            url: '/remisiones/onchange/producto_tabla/' + valor,
            method: 'get',
            success: function(data){
                $(id_co).val(valor).trigger("change.select2");
                $(id_co).removeClass('mi-selector-tabla-producto');

                console.log(data[0]['descripcion_producto']);
            }
        });
    });


    var tbody = $('#lista_productos tbody');
    var fila_contenido = tbody.find('tr').first().html();
    //Agregar fila nueva.

    $(document).on("click", ".button_agregar_producto", function(){
        var fila_nueva = $('<tr><td></td></tr>');
        fila_nueva.append(fila_contenido);
        tbody.append(fila_nueva);
    });

 
    $(document).on("click", ".button_eliminar_producto", function(){    
        $(this).parents('tr').eq(0).remove();
    });
    
    $(document).on("click", ".busqueda", function(){

        $.ajax({
            url: "/reportes/generar/completo",
            type: 'POST',
            data: {
                '_token': $('input[name=_token]').val(),
                'fecha_inicial': $('#fecha_inicial').val(),
                'fecha_final': $('#fecha_final').val(),
                'conferencia': $('#conferencia').val(),
                'categoria': $('#categoria').val(),
                'asociado': $('#asociado').val(),
                'regional': $('#regional').val()
            },
            success: function (data) {
                $("#resultado_reporte").html(data);
            }
        });

    });

    $(document).on("click", ".exportar_pdf", function(){

        $.ajax({
            url: "/reportes/exportar/pdf",
            type: 'POST',
            data: {
                '_token': $('input[name=_token]').val(),
                'fecha_inicial': $('#fecha_inicial').val(),
                'fecha_final': $('#fecha_final').val(),
                'conferencia': $('#conferencia').val(),
                'categoria': $('#categoria').val(),
                'asociado': $('#asociado').val(),
                'regional': $('#regional').val()
            },
            success: function (data) {
                console.log(data);
                //window.open(data, '_blank');
            }
        });

    });

    $(document).on("click", ".exportar_csv", function(){
        alert();
        $.ajax({
            url: "/reportes/exportar/csv",
            type: 'POST',
            data: {
                '_token': $('input[name=_token]').val(),
                'fecha_inicial': $('#fecha_inicial').val(),
                'fecha_final': $('#fecha_final').val(),
                'conferencia': $('#conferencia').val(),
                'categoria': $('#categoria').val(),
                'asociado': $('#asociado').val(),
                'regional': $('#regional').val()
            },
            success: function (data) {
                alert(data);
                window.open(data, '_blank');
            }
        });

    });

    $(document).on("change","#conferencia",function(){
        
        if($(this).val() == null){
            document.getElementById("categoria").disabled = false;
        }else{
            document.getElementById("categoria").disabled = true;    
        }

    });

    $(document).on("change","#categoria",function(){
        
        if($(this).val() == null){
            document.getElementById("conferencia").disabled = false;
        }else{
            document.getElementById("conferencia").disabled = true;    
        }

    });
});