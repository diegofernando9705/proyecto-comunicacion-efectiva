<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
  if(Auth::guest())
    return view('auth.login');                   
  else
    return view('home');

});

Route::get('/remisiones', function () {
  return view('home_remisiones');
});


Auth::routes();

Route::get('/logout', function () {
  Auth::logout();
  return redirect('/');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/role', 'RoleController')->names('role');


Route::resource('/parametros', 'ParametrosRemisionController')->names('parametros');
Route::resource('/conferencias', 'ConferenciasController')->names('conferencias');
Route::resource('/regionales', 'RegionalesController')->names('regionales');

Route::resource('/categorias/conferencias', 'CategoriasConferenciasController')->names('categoriasconferencias');



Route::get('/categorias/conferencias/keyup/{valor?}', 'CategoriasConferenciasController@keyup');
Route::get('/regionales/keyup/{valor?}', 'RegionalesController@keyup');

Route::get('/reportes/completo', 'ReportesController@completo')->name('reportes.completo');

Route::post('/reportes/generar/completo', 'ReportesController@generar');
Route::post('/reportes/exportar/pdf', 'ReportesController@exportarPDF');
Route::post('/reportes/exportar/csv', 'ReportesController@exportarCSV');



Route::resource('/user', 'UserController', ['except' => [
        'create', 'store']])->names('user');
