<!DOCTYPE html>

<html>
    <head>
        <title>Exportar PDF</title>
    </head>
    <body>
    <center>
        <h1>Resultados de la consulta en el Sistema</h1>
    </center>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Identificacion</th>
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Mensaje descripcion</th>
                <th>Mensaje conferencia</th>
                <th>Regional</th>
                <th>Conferencia</th>
                <th>¿Asociado?</th>
            </tr>
        </thead>
        <tbody>
            @foreach($resultado as $report)
                @foreach($report as $total)

                <tr>
                    <td>
                        {{ $total->identificacion }}
                    </td>
                    <td>
                        {{ $total->nombre }}
                    </td>
                    <td>
                        {{ $total->fecha }}
                    </td>
                    <td>
                        {{ $total->descripcion }}
                    </td>
                    <td>
                        {{ $total->conferencia }}
                    </td>
                    <td>
                        {{ $total->regional }}
                    </td>
                    <td>
                        {{ $total->conferencia }}
                    </td>
                    <td>
                        @if($total->asociado == 1)
                        Asociado
                        @else
                        No Asociado
                        @endif
                    </td>
                </tr>

                @endforeach
            @endforeach
        </tbody>
    </table>


    <em style="position: absolute; bottom: 0px;">Desarrollado y Programador por: SoftWorld Colombia</em>
</body>
</html>