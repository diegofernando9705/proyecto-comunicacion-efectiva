<!DOCTYPE html>

<html>
    <head>
        <title>Exportar PDF</title>
    </head>
    <body>
    <center>
        <h1>Resultados de la consulta en el Sistema</h1>
    </center>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>
                    Identificación
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Descripcion
                </th>
                <th>
                    Conferencia
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($resultado as $total)
            <tr>
                <td>
                    {{ $total->identificacion }}
                </td>
                <td>
                    {{ $total->nombre }}
                </td>
                <td>
                    {{ $total->descripcion }}
                </td>
                <td>
                    {{ $total->conferencia }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


    Total: {{ $totales }}
    <em style="position: absolute; bottom: 0px;">Desarrollado y Programador por: SoftWorld Colombia</em>
</body>
</html>
