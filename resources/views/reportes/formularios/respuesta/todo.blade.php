<?php $i=0; ?>

@foreach($resultado as $report)
	@foreach($report as $total)
		
		{{ $i++ }}
		
		<tr>
			<td>
			    {{ $total->identificacion }}
			</td>
			<td>
			    {{ $total->nombre }}
			</td>
			<td>
			    {{ $total->fecha }}
			</td>
			<td>
				{{ $total->descripcion }}
			</td>
			<td>
			    {{ $total->regional }}
			</td>
			<td>
				{{ $total->conferencia }}
			</td>
			<td>
				@if($total->asociado == 1)
					Asociado
				@else
					No Asociado
				@endif
			</td>
		</tr>

	@endforeach
@endforeach

<tr>
	<td colspan="3">
		<b>{{ $frase }} </b>{{ $total_mensajes }} mensajes obtenidos.<br>
		<b>Total registros: </b>{{ $i++ }} registros.<br></td>
</tr>