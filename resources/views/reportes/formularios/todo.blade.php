<div class="row">
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por fechas:</b>
            <hr />
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaInicial" id="fechaInicial">
                <em>Fecha inicial</em>
            </div>
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaFinal" id="fechaFinal">
                <em>Fecha Final</em>
            </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Conferencias:</b>
            <hr />
            <div class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <select class="selectpicker col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12 " multiple data-actions-box="true"  data-live-search="true" id="conferencia" name="conferencia[]">

                    @foreach($lista_simple as $clave)
                    <option value="{{ $clave }}">{{ $clave }}</option>  
                    @endforeach

                </select>
            </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Asociados:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <select class="selectpicker col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12 " multiple data-actions-box="true"  data-live-search="true" id="asociado" name="asociado[]">
                    <option value="1">Asociado</option>
                    <option value="2">No asociado</option>
                </select>
            </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Regionales:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <select class="selectpicker col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12 " data-actions-box="true"  data-live-search="true" name="regional" id="regional" placeholder="Seleccione una opcion">
                    @foreach($regionales as $regional)
                    <option value="{{ $regional->id }}">{{ $regional->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <hr>
    <div class="tabla  col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <table class="table table-bordered table-hover">
            <thead>
            <th>
                Identificación
            </th>
            <th>
                Nombre
            </th>
            <th>
                Fecha
            </th>
            <th>
                Mensaje
            </th>
            <th>
                Regional
            </th>
            <th>
                Conferencia
            </th>
            <th>
                Asociado
            </th>
            </thead>
            <tbody id="resultadoConsulta">

            </tbody>
        </table>

    </div>
</div>

<center>
    <div class="row justify-content-center justify-content-md-start justify-content-center" style="text-align: center; align-items: center; align-content: center;">
        <div class="col-sm-3 col-md-2 col-sm-2 col-lg-2"></div>
        <div class="formularioCSV col-12 col-sm-2 col-md-2 col-sm-2 col-lg-2">
            <form action="{{ url('exportar/csv/todo') }}" method="POST" id="formularioCSVGeneral">
                @csrf
                @method('POST')
                <input type="hidden" name="fechaInicialCSV" id="fechaInicialCSV">
                <input type="hidden" name="fechaFinalCSV" id="fechaFinalCSV">
                <input type="hidden" name="conferenciaCSV[]" id="conferenciaCSV">
                <input type="hidden" name="asociadoCSV[]" id="asociadoCSV">
                <input type="hidden" name="regionalCSV" id="regionalCSV">

                <button type="submit" class="btn btn-info">Exportar CSV</button>
            </form>
        </div>
        <div class="botonSubmit col-sm-4 col-md-4 col-sm-4 col-lg-4">
            <button type="button" class="btn btn-success btn-buscar float-center" style="margin-left: 20px; margin-right: 20px;">Generar reporte</button>
        </div>
        <div class="formularioPDF col-sm-2 col-md-2 col-sm-2 col-lg-2">
            <form action="{{ url('exportar/pdf/todo') }}" method="POST">
                @csrf
                @method('POST')
                <input type="hidden" name="fechaInicialPDF" id="fechaInicialPDF">
                <input type="hidden" name="fechaFinalPDF" id="fechaFinalPDF">
                <input type="hidden" name="conferenciaPDF[]" id="conferenciaPDF">
                <input type="hidden" name="asociadoPDF[]" id="asociadoPDF">
                <input type="hidden" name="regionalPDF" id="regionalPDF">

                <button type="submit" class="btn btn-danger">Exportar PDF</button>
            </form>
        </div>
        <div class="col-sm-3 col-md-2 col-sm-2 col-lg-2"></div>
    </div>
</center>

<script type="text/javascript">

    $(function () {
        $('.selectpicker').selectpicker();
    });

</script>