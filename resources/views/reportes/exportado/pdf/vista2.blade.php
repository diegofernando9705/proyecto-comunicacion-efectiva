<!-- ARCHIVO VAR VER LOS RESULTADOS SIN SELECCIONAR ALGUNA OPCION EN EL PANEL DE BUSQUEDA. TODOS LOS CAMPOS SE ENCUENTRAN VACIOS -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>Reporte</title>
		
		<style type="text/css">
			thead:before, thead:after { 
				display: none; 
			}

			tbody:before, tbody:after { 
				display: none; 
			}
		</style>

	</head>
	<body>

		<div class="contenido" style="width: 100%;">
			<div style="width: 60%; float:left; border: 1px solid black; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; text-align: center;">
				<img src="../public/img/logo_more_small_CE.png" width="150px">
			</div>
			<div style="text-align:center; width: 30%; float: right; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;">
				<b>Fecha de reporte:</b><br>
				<?php echo date('Y-m-d'); ?>
			</div>
		</div>
		<div style="clear: both"></div>
		
		<p style="color: white;">salto de linea</p>

		<div class="contenido" style="width: 100%;">

			<p style="color: white;">salto de linea</p>

			<table id="lista_productos"  style="width: 100%; border: 1px solid black; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: -30px;">
				<thead>
					<tr>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Cédula</th>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Nombre</th>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Apellido</th>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Conferencia</th>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Fecha de acceso</th>

				</tr>
				</thead>
				<tbody>
					@foreach($reportes as $reporte)
						<tr>
							<td>{{ $reporte->identificacion_persona }}</td>
							<td>{{ $reporte->nombres_persona }}</td>
							<td>{{ $reporte->apellidos_persona }}</td>
							<td>{{ $reporte->descripcion_conferencia }}</td>
							<td>{{ $reporte->fecha_acceso }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>

			<table id="lista_productos"  style="width: 100%; border: 1px solid black; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: 10px;">
				<thead>
					<tr>
						<th style="border-bottom: 1px solid black; padding-bottom: 5px;">Total de mensajes</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{ $mensajes_totales }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</body>
</html>
