@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de reportes</h3>
                  <p class="card-category"></p>
                </div>
                <form action="{{ url('/reportes/exportar/csv') }}" id="formulario_completo" method="POST">
                    @csrf
                    @method('POST')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <label style="padding-bottom: 10px;"><b>Rango por fechas:</b></label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" name="fecha_inicial" id="fecha_inicial">
                                            <small class="form-text text-muted">Fecha inicial.</small>
                                        </div>
                                        <div class="form-group">
                                            <input type="date" class="form-control"  name="fecha_final" id="fecha_final">
                                            <small class="form-text text-muted">Fecha final</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <label style="padding-bottom: 10px;"><b>B&uacute;squeda por conferencia:</b></label>
                                        <div class="form-group">
                                            <select class="form-control mi-selector" multiple="" name="conferencia[]" id="conferencia">
                                                    <option value="0"></option>
                                                @foreach($conferencias as $conferencia)
                                                    <option value="{{ $conferencia->id_conferencia }}">{{ $conferencia->descripcion_conferencia }}</option>
                                                @endforeach
                                            </select>
                                            <small class="form-text text-muted">Busca por tu conferencia favorita.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <label style="padding-bottom: 10px;"><b>B&uacute;squeda por categoria conferencia:</b></label>
                                        <div class="form-group">
                                            <select class="form-control mi-selector" multiple="" name="categoria[]" id="categoria">
                                                <option value="0"></option>
                                                @foreach($categorias as $categoria)
                                                    <option value="{{ $categoria->id_categoria }}">{{ $categoria->descripcion_categoria }}</option>
                                                @endforeach
                                            </select>
                                            <small class="form-text text-muted">Busca por tu conferencia favorita.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <label style="padding-bottom: 10px;"><b>B&uacute;squeda por Asociado:</b></label>
                                        <div class="form-group">
                                            <select class="form-control mi-selector" name="asociado" id="asociado">
                                                <option value="0" selected=""></option>
                                                <option value="1">Soy asociado</option>
                                                <option value="2">No soy asociado</option>
                                                <option value="0">Cualquiera</option>
                                            </select>
                                            <small class="form-text text-muted">Busca segun asociado.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" style="margin-top: 5px;">
                                <div class="card">
                                    <div class="card-body">
                                        <label style="padding-bottom: 10px;"><b>B&uacute;squeda por Regional:</b></label>
                                        <div class="form-group">
                                            <select class="form-control mi-selector" name="regional" id="regional">
                                                    <option value="0" selected=""></option>
                                                @foreach($regionales as $regional)
                                                    <option value="{{ $regional->id_regional }}">{{ $regional->descripcion_regional }}</option>
                                                @endforeach
                                            </select>
                                            <small class="form-text text-muted">Buscar por la Regional.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" style="margin-top: 5px; text-align: center;">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info busqueda" data-info="busqueda">Buscar</button>
                                        <button type="button" class="btn btn-danger exportar_pdf" data-info="busqueda">Descargar PDF</button>
                                        <button type="submit" class="btn btn-success" data-info="busqueda">Descargar CSV</button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </form>
                <div id="resultado_reporte"></div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
