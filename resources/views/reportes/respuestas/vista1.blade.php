<!-- ARCHIVO VAR VER LOS RESULTADOS SIN SELECCIONAR ALGUNA OPCION EN EL PANEL DE BUSQUEDA. TODOS LOS CAMPOS SE ENCUENTRAN VACIOS -->

<table class="table table-hovered table-bordered">
	<thead>
		<th scope="col">Identificacion</th>
		<th scope="col">Nombre</th>
		<th scope="col">Apellido</th>
		<th scope="col">Conferencia</th>
		<th scope="col">Fecha de acceso</th>
	</thead>
	<tbody>
		@foreach($reportes as $reporte)
			<tr>
				<td>{{ $reporte->identificacion_persona }}</td>
				<td>{{ $reporte->nombres_persona }}</td>
				<td>{{ $reporte->apellidos_persona }}</td>
				<td>{{ $reporte->descripcion_conferencia }}</td>
				<td>{{ $reporte->fecha_acceso }}</td>
			</tr>
		@endforeach		
	</tbody>

</table>


<script>
    
$(document).ready(function(){

     $(document).on('click', '.pagination a', function(event){
          event.preventDefault(); 
          var page = $(this).attr('href').split('page=')[1];
          fetch_data(page);
     });

     function fetch_data(page){
        $.ajax({
            url:"/reportes/completo?page="+page,
            success:function(data)
            {
                $('#resultado_reporte').html(data);
            }
        });
     }
 });

</script>
