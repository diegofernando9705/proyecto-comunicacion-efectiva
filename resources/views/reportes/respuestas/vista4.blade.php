<!-- ARCHIVO VAR VER LOS RESULTADOS SIN SELECCIONAR ALGUNA OPCION EN EL PANEL DE BUSQUEDA. TODOS LOS CAMPOS SE ENCUENTRAN VACIOS -->

<table class="table table-hovered table-bordered">
	<thead>
		<th scope="col">Identificacion</th>
		<th scope="col">Nombre</th>
		<th scope="col">Apellido</th>
		<th scope="col">Conferencia</th>
		<th scope="col">Fecha de acceso</th>
	</thead>
	<tbody>
		@foreach($reportes as $rep)
			@foreach($rep as $reporte)
			<tr>
				<td> {{ $reporte->identificacion_persona }}</td>
				<td> {{ $reporte->nombres_persona }}</td>
				<td> {{ $reporte->apellidos_persona }}</td>
				<td> {{ $reporte->descripcion_conferencia }}</td>
				<td> {{ $reporte->fecha_acceso }}</td>
			</tr>
			@endforeach
		@endforeach		
	</tbody>

</table>