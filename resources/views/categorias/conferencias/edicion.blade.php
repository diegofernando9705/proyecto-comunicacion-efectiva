@foreach($categorias as $categoria)
<div class="container">
	 @include('custom.parameters')

	 {!! Form::open(['route' => array('categoriasconferencias.update', $categoria->id_categoria), 'files' => true]) !!}

		@csrf
		@method('PUT')
	
		<div class="form-group">
			<div class="row">
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Nombre categoria:</b></label>
					<input type="text" class="form-control" name="descripcion_categoria" placeholder="Nombre categoria" required="" value="{{ $categoria->descripcion_categoria }}">
				</div>
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Estado categoria:</b></label>
					<select class="form-control" name="estado_categoria">
						@if($categoria->estado_categoria == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1">Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif
					</select>
				</div>
			</div>
		</div>
		

		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-ban"></i> Cancelar actualizacion
				</button>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar
				</button>
			</center>
		</div>

	{!! Form::close() !!}
</div>
@endforeach