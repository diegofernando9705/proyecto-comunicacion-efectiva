<thead>
	                    		<tr>
	                    			<th scope="col">ID</th>
	                    			<th scope="col">Nombre categoria</th>
	                    			<th scope="col">Estado categoria</th>
	                    			<th colspan="3"><center>Acciones</center></th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		@foreach($categorias as $categoria)
	                    			<tr>
	                    				<td>{{ $categoria->id_categoria }}</td>
	                    				<td>{{ $categoria->descripcion_categoria }}</td>
	                    				@if($categoria->estado_categoria == '1')
		                                    <td class="alert alert-success">Activo</td>
		                                @else
		                                    <td class="alert alert-danger">Inactivo</td>
		                                @endif
		                                <td style="text-align: center;">
		                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $categoria->id_categoria }}" data-url="{{ route('categoriasconferencias.edit', $categoria->id_categoria ) }}"><i class="far fa-edit"></i></button>
		                                </td>
		                                <td>
		                                    <center>
		                                    <form method="POST" action="{{ route('categoriasconferencias.destroy', $categoria->id_categoria)}}">
		                                        @csrf
		                                        @method('DELETE')
		                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
		                                            <i class="fas fa-calendar-minus"></i>
		                                        </button>
		                                    </form>
		                                    </center>
		                                </td>
	                    			</tr>
	                    		@endforeach                    		
	                    	</tbody>