<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bienvenido | Panel de Administrador</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <link href="{{ asset('bootstrap4/css/style-starter.css') }}" rel="stylesheet" />

  <link href="{{ asset('bootstrap4/icons/all.css') }}" rel="stylesheet" />
  <link href="{{ asset('bootstrap4/icons/fontawesome.css.css') }}" rel="stylesheet" />

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

</head>

<body>

  <section>
    <div class="sidebar-menu sticky-sidebar-menu">
      <div class="logo">
        <a href="{{ url('/remisiones') }}">
          <img src="{{ asset('img/logo_more_small_CE.png') }}" width="95%" />
        </a>
      </div>

      <div class="logo-icon text-center">
        <a href="{{ url('/remisiones') }}" title="logo">
          <img src="{{ asset('img/logo_more_small_CE.png') }}" width="95%" />
        </a>
      </div>

      <div class="sidebar-menu-inner">
        <ul class="nav nav-pills nav-stacked custom-nav">
          <li class=" {{ Request::path() == '/' ? ' nav-active' : '' }}">
            <a href="{{ url('/') }}">
              
              <span>
                Principal
              </span>
            </a>
          </li>

          <li class="menu-list {{ Request::path() == 'reportes' ? ' nav-active' : '' }} {{ Request::path() == 'reportes/completo' ? ' nav-active' : '' }}">
            <a href="#">
              <i class="fas fa-chart-pie"></i>
              <span>
                Reportes
                <i class="lnr lnr-chevron-right">
                </i>
              </span>
            </a>
<!--
            <ul class="sub-menu-list">
              <li>
                <a href="{{ url('reportes/index') }}" class="nav-link">
                  Historial de Accesos
                </a>
              </li>
            </ul>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ url('reportes/create') }}" class="nav-link">
                  Mensajes
                </a>
              </li>
            </ul>-->

            <ul class="sub-menu-list">
              <li>
                <a href="{{ url('reportes/completo') }}" class="nav-link">
                  Completo
                </a>
              </li>
            </ul>
          </li>

          <li class="menu-list {{ Request::path() == 'conferencias' ? ' nav-active' : '' }} {{ Request::path() == 'conferencias/create' ? ' nav-active' : '' }}">
            <a href="#">
              <i class="fas fa-chalkboard-teacher"></i>
              <span>
                Conferencias
                <i class="lnr lnr-chevron-right">
                </i>
              </span>
            </a>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('conferencias.create') }}" class="nav-link">
                  Crear
                </a>
              </li>
            </ul>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('conferencias.index') }}" class="nav-link">
                  Listar
                </a>
              </li>
            </ul>
          </li>

          <li class="menu-list {{ Request::path() == 'categorias/conferencias' ? ' nav-active' : '' }} {{ Request::path() == 'categorias/conferencias/create' ? ' nav-active' : '' }}">
            <a href="#">
              <i class="fas fa-network-wired"></i>
              <span>
                Categoria conf
                <i class="lnr lnr-chevron-right">
                </i>
              </span>
            </a>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('categoriasconferencias.create') }}" class="nav-link">
                  Crear
                </a>
              </li>
            </ul>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('categoriasconferencias.index') }}" class="nav-link">
                  Listar
                </a>
              </li>
            </ul>
          </li>

          <li class="menu-list {{ Request::path() == 'regionales' ? ' nav-active' : '' }} {{ Request::path() == 'regionales/create' ? ' nav-active' : '' }}">
            <a href="#">
              <i class="fas fa-street-view"></i>
              <span>
                Regionales
                <i class="lnr lnr-chevron-right">
                </i>
              </span>
            </a>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('regionales.create') }}" class="nav-link">
                  Crear
                </a>
              </li>
            </ul>

            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('regionales.index') }}" class="nav-link">
                  Listar
                </a>
              </li>
            </ul>
          </li>

          <li class="active">
            <a href="#">
              <span>
                Configuracion Avanzada
              </span>
            </a>
            <li class="menu-list {{ Request::path() == 'user' ? ' nav-active' : '' }} {{ Request::path() == 'user' ? ' nav-active' : '' }}">
              @can('haveaccess','user.index')
                <a href="#">
                  <i class="fas fa-child"></i>
                  <span>
                    Modulo de Usuarios
                    <i class="lnr lnr-chevron-right"></i>
                  </span>
                </a>
                <ul class="sub-menu-list">
                  <li>
                    <a href="{{route('user.index')}}" class="nav-link">
                      Listar
                    </a>
                  </li>
                </ul>
              @endcan
            </li>

            <li class="menu-list {{ Request::path() == 'role' ? ' nav-active' : '' }} {{ Request::path() == 'role' ? ' nav-active' : '' }}">
              @can('haveaccess','role.index')
                <a href="#">
                  <i class="fas fa-users-cog"></i>
                  <span>
                    Modulo Roles
                    <i class="lnr lnr-chevron-right"></i>
                  </span>
                </a>

                <ul class="sub-menu-list">
                  <li>
                    <a href="{{route('role.index')}}" class="nav-link">
                      Listar
                    </a>
                  </li>
                </ul>
              @endcan
            </li>

            <li class="menu-list {{ Request::path() == 'remisiones/parametros' ? ' nav-active' : '' }} {{ Request::path() == 'remisiones/parametros/create' ? ' nav-active' : '' }}">
                <a href="#">
                  <i class="fas fa-fax"></i>
                  <span>
                    Par&aacute;metros Empresa
                    <i class="lnr lnr-chevron-right"></i>
                  </span>
                </a>
                <ul class="sub-menu-list">
                  <li>
                    <a href="{{route('parametros.create')}}" class="nav-link">
                      Crear
                    </a>
                  </li>
                </ul>
                <ul class="sub-menu-list">
                  <li>
                    <a href="{{route('parametros.index')}}" class="nav-link">
                      Listar
                    </a>
                  </li>
                </ul>
            </li>

          </li>
        </ul>

        <a class="toggle-btn">
          <i class="fa fa-angle-double-left menu-collapsed__left">
            <span>
              Contraer
            </span>
          </i>
          <i class="fa fa-angle-double-right menu-collapsed__right"></i>
        </a>
      </div>
      
    </div>

    <div class="header sticky-header">
      <div class="menu-right">
        <div class="navbar user-panel-top">
          <div class="search-box">
            <form action="#search-results.html" method="get">
              <input class="search-input" id="search" placeholder="Búsqueda rápida..." type="search" />
              <button class="search-submit" value="">
                <span class="fa fa-search"></span>
              </button>
            </form>
          </div>

          <div class="user-dropdown-details d-flex">
            <div class="profile_details_left">
              <ul class="nofitications-dropdown">
                <li class="dropdown">
                  <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge blue">
                      3
                    </span>
                  </a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="notification_header">
                        <h3>
                          Tu tienes notificaciones
                        </h3>
                      </div>
                    </li>
                    <li>
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar1.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Johnson purchased template
                          </p>
                          <span>
                            Reciente
                          </span>
                        </div>
                      </a>
                    </li>

                    <li class="odd">
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar2.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Nueva remision en el sistema
                          </p>
                          <span>
                            Hace 1 hora
                          </span>
                        </div>
                      </a>
                    </li>

                    <li>
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar3.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Un usuario ha sido creado
                          </p>
                          <span>
                            Hace 12 horas
                          </span>
                        </div>
                      </a>
                    </li>

                    <li>
                      <div class="notification_bottom">
                        <a class="bg-primary" href="#all">
                          Ver todas
                        </a>
                      </div>
                    </li>

                  </ul>
                </li>

                <li class="dropdown">
                  <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-comment-o"></i>
                    <span class="badge blue">
                      4
                    </span>
                  </a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="notification_header">
                        <h3>
                          Usted tiene 4 mensajes nuevos
                        </h3>
                      </div>
                    </li>

                    <li>
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar1.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Johnson purchased template
                          </p>
                          <span>
                            Reciente
                          </span>
                        </div>
                      </a>
                    </li>

                    <li class="odd">
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar2.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Isabella Garcia escribio
                          </p>
                          <span>
                            Hace 1 hora
                          </span>
                        </div>
                      </a>
                    </li>

                    <li>
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar3.jpg"/>
                        </div>
                        <div class="notification_desc">
                          <p>
                            Lorem ipsum dolor sit amet
                          </p>
                          <span>
                            Hace 1 hora
                          </span>
                        </div>
                      </a>
                    </li>

                    <li>
                      <a class="grid" href="#">
                        <div class="user_img">
                          <img alt="" src="assets/images/avatar1.jpg"/>
                        </div>

                        <div class="notification_desc">
                          <p>
                            Johnson purchased template
                          </p>
                          <span>
                            Justo ahora
                          </span>
                        </div>
                      </a>
                    </li>

                    <li>
                      <div class="notification_bottom">
                        <a class="bg-primary" href="#all">
                          Ver todos los mensajes
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>

            <div class="profile_details">
              <ul>
                <li class="dropdown profile_details_drop">
                  <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenu3">
                    <div class="profile_img">
                      <img alt="" class="rounded-circle" src="assets/images/profileimg.jpg"/>
                      <div class="user-active">
                        <span>
                        </span>
                      </div>
                    </div>
                  </a>

                  <ul aria-labelledby="dropdownMenu3" class="dropdown-menu drp-mnu">
                    <li class="user-info">
                      <h5 class="user-name">
                        {{ auth()->user()->name }}
                      </h5>
                      <span class="status ml-2">
                        Available
                      </span>
                    </li>

                    <li>
                      <a href="#">
                        <i class="lnr lnr-user">
                        </i>
                        Perfil
                      </a>
                    </li>

                    <li>
                      <a href="#">
                        <i class="lnr lnr-users">
                        </i>
                        1k Followers
                      </a>
                    </li>

                    <li>
                      <a href="#">
                        <i class="lnr lnr-cog">
                        </i>
                        Configuración
                      </a>
                    </li>

                    <li>
                      <a href="#">
                        <i class="lnr lnr-heart">
                        </i>
                        100 Likes
                      </a>
                    </li>

                    <li class="logout">
                      <a href="{{ url('/logout') }}">
                        <i class="fa fa-power-off">
                        </i>
                        Cerrar sesión
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div> <!-- FIN user-dropdown-details d-flex -->
        </div>
      </div>
    </div>

    <div class="main-content">
      <div class="container-fluid content-top-gap">
        @yield('content')
        <!-- MODALES VER, EDICION Y ELIMINAR -->
        <div class="modal fade bd-example-modal-lg" id="ver_modales" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content" id="modal-body">
                
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</body>
  
   <!--footer section start-->
<footer class="dashboard">
  <p>&copy 2020 Collective. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank"
      class="text-primary">W3layouts.</a></p>
</footer>
<!--footer section end-->
<!-- move top -->
<button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button>
<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    scrollFunction()
  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("movetop").style.display = "block";
    } else {
      document.getElementById("movetop").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
<!-- /move top -->


<script src="{{ asset('bootstrap4/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('bootstrap4/js/jquery-1.10.2.min.js') }}"></script>

<!-- chart js -->
<script src="{{ asset('bootstrap4/js/Chart.min.js') }}"></script>
<script src="{{ asset('bootstrap4/js/utils.js') }}"></script>
<!-- //chart js -->

<!-- Different scripts of charts.  Ex.Barchart, Linechart -->
<script src="{{ asset('bootstrap4/js/bar.js') }}"></script>
<script src="{{ asset('bootstrap4/js/linechart.js') }}"></script>
<!-- //Different scripts of charts.  Ex.Barchart, Linechart -->


<script src="{{ asset('bootstrap4/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('bootstrap4/js/scripts.js') }}"></script>

<!-- close script -->
<script>
  var closebtns = document.getElementsByClassName("close-grid");
  var i;

  for (i = 0; i < closebtns.length; i++) {
    closebtns[i].addEventListener("click", function () {
      this.parentElement.style.display = 'none';
    });
  }
</script>
<!-- //close script -->

<!-- disable body scroll when navbar is in active -->
<script>
  $(function () {
    $('.sidebar-menu-collapsed').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll when navbar is in active -->

 <!-- loading-gif Js -->
 <script src="{{ asset('bootstrap4/js/modernizr.js') }}"></script>
 <script>
     $(window).load(function () {
         // Animate loader off screen
         $(".se-pre-con").fadeOut("slow");;
     });
 </script>
 <!--// loading-gif Js -->

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('bootstrap4/js/bootstrap.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

  <script type="text/javascript" src="{{ asset('bootstrap4/js/acciones.js') }}"></script>

  @yield('scripts')
</html>
