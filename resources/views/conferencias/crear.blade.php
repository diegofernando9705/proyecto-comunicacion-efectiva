@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Crear conferencia</h3>
                </div>

                <div class="card-body">
                    <div class="alert alert-danger">
                        <b>TODOS</b> los campos marcados con asteriscos (*) son obligatorios.
                    </div>
                    @include('custom.message')
                    <form action="{{ route('conferencias.store') }}" method="POST">
                        @csrf
                        @method('POST')


                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label style="padding-bottom: 3px;"><b>(*) Categoria conferencia:</b></label>

                                    <select class="form-control mi-selector" name="categoria" required="">
                                        <option value="">Seleccionar regional</option>
                                        @foreach($categorias_conferencias as $categoria)
                                            <option value="{{ $categoria->id_categoria }}">{{ $categoria->descripcion_categoria }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label style="padding-bottom: 3px;"><b>(*) Nombre conferencia:</b></label>
                                    <input type="text" class="form-control" name="descripcion_conferencia" placeholder="Nombre categoria" required="">
                                </div>
                                <div class="col">
                                    <label style="padding-bottom: 3px;"><b>(*) Estado conferencia:</b></label>
                                    <select class="form-control" name="estado_conferencia" required="">
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
                            <center>
                                <a href="{{ route('conferencias.index') }}">
                                <button class="btn btn-danger" type="button">
                                    <i class="fas fa-ban"></i> Cancelar conferencia
                                </button>
                                </a>
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-paper-plane"></i> Registrar conferencia
                                </button>
                            </center>
                        </div>
                    </form>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
