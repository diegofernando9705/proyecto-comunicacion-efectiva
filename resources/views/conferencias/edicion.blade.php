@foreach($conferencias as $conferencia)
<div class="container">
	 @include('custom.parameters')
	  {!! Form::open(['route' => array('conferencias.update', $conferencia->id_conferencia), 'files' => true]) !!}
	  	
		@csrf
		@method('PUT')

		<div class="form-group">
			<div class="row">
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Categoria conferencia:</b></label>
					<select class="form-control mi-selector" name="categoria" required="">
						<option value="">Seleccionar regional</option>
						@foreach($categorias_conferencias as $categoria)
							@if($categoria->id_categoria == $conferencia->id_categoria)
								<option value="{{ $categoria->id_categoria }}" selected="">{{ $categoria->descripcion_categoria }}</option>
							@else
								<option value="{{ $categoria->id_categoria }}">{{ $categoria->descripcion_categoria }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Nombre conferencia:</b></label>
					<input type="text" class="form-control" name="descripcion_conferencia" placeholder="Nombre categoria" required="" value="{{ $conferencia->descripcion_conferencia }}">
				</div>
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Estado conferencia:</b></label>
					<select class="form-control mi-selector" name="estado_conferencia" required="">
						@if($conferencia->estado_conferencia == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1" >Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif

					</select>
				</div>
			</div>
		</div>

		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal">
					<i class="fas fa-ban"></i> Cancelar conferencia
				</button>
				</a>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar conferencia
				</button>
			</center>
		</div>
	{!! Form::close() !!}
</div>
<script type="text/javascript">
	    $(document).ready(function () {
        $('.mi-selector').select2();
        $('.mi-selector-tabla').select2();
        $('.mi-selector-tabla-producto').select2();

    });

</script>
@endforeach