@extends('layouts.app')

@section('content')
<div class="content" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Listado de las conferencias</h3>
                </div>

                <div class="card-body">
                    @include('custom.message')
                    <div class="table-responsive">
	                    <div class="form-group">
	                        <label><b>Búsqueda de una conferencia...</b></label>
	                        <input type="text" class="form-control" id="busqueda_conferencia" name="busqueda_conferencia" placeholder="Digite descripcion conferencia">
	                    </div>
	                    <table class="table table-hover table-bordered" id="resultado_consulta">
	                    	<thead>
	                    		<tr>
	                    			<th scope="col">ID</th>
	                    			<th scope="col">Nombre conferencia</th>
	                    			<th scope="col">Categoria conferencia</th>
	                    			<th scope="col">Estado conferencia</th>
	                    			<th colspan="3"><center>Acciones</center></th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		@foreach($conferencias as $conferencia)
	                    			<tr>
	                    				<td>{{ $conferencia->id_conferencia }}</td>
	                    				<td>{{ $conferencia->descripcion_conferencia }}</td>
	                    				<td>{{ $conferencia->descripcion_categoria }}</td>
	                    				@if($conferencia->estado_conferencia == '1')
		                                    <td class="alert alert-success">Activo</td>
		                                @else
		                                    <td class="alert alert-danger">Inactivo</td>
		                                @endif
		                                <td style="text-align: center;">
		                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $conferencia->id_conferencia }}" data-url="{{ route('conferencias.edit', $conferencia->id_conferencia ) }}"><i class="far fa-edit"></i></button>
		                                </td>
		                                <td>
		                                    <center>
		                                    <form method="POST" action="{{ route('conferencias.destroy', $conferencia->id_conferencia)}}">
		                                        @csrf
		                                        @method('DELETE')
		                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
		                                            <i class="fas fa-calendar-minus"></i>
		                                        </button>
		                                    </form>
		                                    </center>
		                                </td>
	                    			</tr>
	                    		@endforeach                    		
	                    	</tbody>
	                    </table>
	                    {{ $conferencias->links() }}
                	</div>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
