@extends('layouts.app')

@section('content')
<div class="content" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Listar regionales</h3>
                </div>

                <div class="card-body">
                    @include('custom.message')
                    <div class="table-responsive">
	                    <div class="form-group">
	                        <label><b>Búsqueda de una regional...</b></label>
	                        <input type="text" class="form-control" id="busqueda_regional" name="busqueda_regional" placeholder="Digite descripcion regional">
	                    </div>
	                    <table class="table table-hover table-bordered" id="resultado_consulta">
	                    	<thead>
	                    		<tr>
	                    			<th scope="col">ID</th>
	                    			<th scope="col">Nombre regional</th>
	                    			<th scope="col">Estado regional</th>
	                    			<th colspan="3"><center>Acciones</center></th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		@foreach($regionales as $regional)
	                    			<tr>
	                    				<td>{{ $regional->id_regional }}</td>
	                    				<td>{{ $regional->descripcion_regional }}</td>
	                    				@if($regional->estado_regional == '1')
		                                    <td class="alert alert-success">Activo</td>
		                                @else
		                                    <td class="alert alert-danger">Inactivo</td>
		                                @endif
		                                <td style="text-align: center;">
		                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $regional->id_regional }}" data-url="{{ route('regionales.edit', $regional->id_regional ) }}"><i class="far fa-edit"></i></button>
		                                </td>
		                                <td>
		                                    <center>
		                                    <form method="POST" action="{{ route('regionales.destroy', $regional->id_regional)}}">
		                                        @csrf
		                                        @method('DELETE')
		                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
		                                            <i class="fas fa-calendar-minus"></i>
		                                        </button>
		                                    </form>
		                                    </center>
		                                </td>
	                    			</tr>
	                    		@endforeach                    		
	                    	</tbody>
	                    </table>
	                    {{ $regionales->links() }}
                	</div>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
