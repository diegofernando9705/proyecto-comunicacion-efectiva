<thead>
	                    		<tr>
	                    			<th scope="col">ID</th>
	                    			<th scope="col">Nombre regional</th>
	                    			<th scope="col">Estado regional</th>
	                    			<th colspan="3"><center>Acciones</center></th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		@foreach($regionales as $regional)
	                    			<tr>
	                    				<td>{{ $regional->id_regional }}</td>
	                    				<td>{{ $regional->descripcion_regional }}</td>
	                    				@if($regional->estado_regional == '1')
		                                    <td class="alert alert-success">Activo</td>
		                                @else
		                                    <td class="alert alert-danger">Inactivo</td>
		                                @endif
		                                <td style="text-align: center;">
		                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $regional->id_regional }}" data-url="{{ route('regionales.edit', $regional->id_regional ) }}"><i class="far fa-edit"></i></button>
		                                </td>
		                                <td>
		                                    <center>
		                                    <form method="POST" action="{{ route('regionales.destroy', $regional->id_regional)}}">
		                                        @csrf
		                                        @method('DELETE')
		                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
		                                            <i class="fas fa-calendar-minus"></i>
		                                        </button>
		                                    </form>
		                                    </center>
		                                </td>
	                    			</tr>
	                    		@endforeach                    		
	                    	</tbody>