@foreach($regionales as $regional)
<div class="container">
	 @include('custom.parameters')
	 
	 {!! Form::open(['route' => array('regionales.update', $regional->id_regional), 'files' => true]) !!}

		@csrf
		@method('PUT')
	
		<div class="form-group">
			<div class="row">
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Nombre regional:</b></label>
					<input type="text" class="form-control" name="descripcion_regional" placeholder="Nombre regional" required="" value="{{ $regional->descripcion_regional }}">
				</div>
				<div class="col">
					<label style="padding-bottom: 3px;"><b>(*) Estado regional:</b></label>
					<select class="form-control" name="estado_regional">
						@if($regional->estado_regional == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1">Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif
					</select>
				</div>
			</div>
		</div>
		

		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-ban"></i> Cancelar actualizacion
				</button>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar
				</button>
			</center>
		</div>

	{!! Form::close() !!}
</div>
@endforeach