@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Crear regionales</h3>
                </div>

                <div class="card-body">
                	<div class="alert alert-danger">
                		<b>TODOS</b> los campos marcados con asteriscos (*) son obligatorios.
                	</div>
                    
                    @include('custom.message')

                    <form action="{{ route('regionales.store') }}" method="POST">
                    	@csrf
                    	@method('POST')

                    	<div class="form-group">
                    		<div class="row">
                    			<div class="col">
                    				<label style="padding-bottom: 3px;"><b>(*) Nombre regional: </b></label>
                    				<input type="text" class="form-control" name="descripcion_regional" placeholder="Nombre regional" required="">
                    			</div>
                    			<div class="col">
                    				<label style="padding-bottom: 3px;"><b>(*) Estado regional: </b></label>
                    				<select class="form-control" name="estado_regional">
                    					<option value="1">Activo</option>
                    					<option value="0">Inactivo</option>
                    				</select>
                    			</div>
                    		</div>
                    	</div>

                    	<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
                            <center>
                            	<a href="{{ route('regionales.index') }}">
                                <button class="btn btn-danger" type="button">
                                    <i class="fas fa-ban"></i> Cancelar registro
                                </button>
                            	</a>
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-paper-plane"></i> Registrar regional
                                </button>
                            </center>
                        </div>
                    </form>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
