<div class="row" style="border: 1px solid #E7EDF6; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px;">
	<div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10" style="border-left: 1px solid #E7EDF6; border-right: 1px solid #E7EDF6;">
		<p align="justify" style="text-align: center; font-weight: bold; line-height:18px;">
			<img src="{{ asset('img/logo_more_small_CE.png') }}" width="300px" />
		</p>
	</div>
	<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
		<p align="justify" style="text-align: center; font-weight: bold; line-height:18px;">
			<?php echo date('d-m-Y'); ?>
		</p>
	</div>
</div>
