@foreach($parametros as $parametro)
<div class="container">
	 @include('custom.parameters')
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
				<select class="form-control" disabled="">
					<option value="" selected="">Seleccione un tipo de identificación</option>
					@foreach($tipo_identificacion as $tipo)
						@if($parametro->id_tipo_parametro == $tipo->id_tipo)
							<option value="{{ $tipo->id_tipo }}" selected="">{{ $tipo->descripcion_tipo }}</option>
						@else
							<option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
						@endif
					@endforeach
				</select>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
				<input type="text" class="form-control" placeholder=""  value="{{ $parametro->identificacion_parametros }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Logo compañ&iacute;a:</b></label>
				<img src="{{ asset('storage/'.$parametro->logo_parametros) }}" width="100%">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Raz&oacute;n social:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->nombre_parametros }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Direcci&oacute;n  compañ&iacute;a:</b></label>
				<input type="text" class="form-control" placeholder="" name="direccion" value="{{ $parametro->direccion_parametros }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Ciudad compañ&iacute;a:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->id_ciudad_parametros }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Tel&eacute;fono fijo:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->telefono_fijo_parametros }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->celular_1_parametros }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Celular 1:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->celular_2_parametros }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Celular 2:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->celular_3_parametros }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Correo electr&oacute;nico:</b></label>
				<input type="text" class="form-control" placeholder="" value="{{ $parametro->email_cliente }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Estado par&aacute;metro:</b></label>
				<select class="form-control"  name="estado" disabled="">
						@if($parametro->estado_parametro == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1">Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif
						
					</select>
			</div>
		</div>
	</div>

</div>
@endforeach