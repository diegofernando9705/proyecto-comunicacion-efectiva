<div class="container">
	 @include('custom.parameters')
</div>
@foreach($remision_unica as $remision_uni)
	<div class="container">
		<div class="table-responsive">
			<!-- NUMERO DE REMISION -->
			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-7 col-md-7 col-lg-7" style="text-align: center;"></div>
					<div class="col-12 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-6 col-form-label">
								<b>REMISION No.</b>
							</label>
							<div class="col-sm-6">
								<input type="number" name="codigo_remision" class="form-control" id="staticEmail" value="{{ $remision_uni->id_consecutivo }}" disabled="" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- CLIENTE Y FECHA -->
			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">

					<div class="col-12 col-sm-7 col-md-7 col-lg-7" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
						<center>
							<h4><b>Informaci&oacute;n del cliente</b></h4>
						</center>
						<br>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Nombre</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" disabled="" value="{{ $remision_uni->nombres_cliente }}">		
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Nit. / C.C</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="identificacion_formulario" value="{{ $remision_uni->identificacion }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Direccion:</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="direccion_formulario" value="{{ $remision_uni->direccion_cliente }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Ciudad:</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="ciudad_formulario" value="{{ $remision_uni->ciudad_cliente }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Telefono:</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="telefono_formulario" value="{{ $remision_uni->telefono_fijo_cliente }} - {{ $remision_uni->celular_1_cliente }} - {{ $remision_uni->celular_2_cliente }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Email:</b>
							</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="email_formulario" value="{{ $remision_uni->email_cliente }}" disabled="" />
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-5 col-md-5 col-lg-5" style=" padding-top:30px; text-align: center; border: 1px solid black; border-radius: 15px; ">
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>FECHA</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="fecha_remision" value="{{ $remision_uni->fecha }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Vence</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control-plaintext" name="vence_remision" value="{{ $remision_uni->vence }}" disabled="" />
							</div>
						</div>
						<div class="form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">
								<b>Orden:</b>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="orden_remision" value="{{ $remision_uni->orden }}" disabled="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endforeach
			

<div class="container">
	<!-- PRODUCTOS -->
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
				<center>
					<h4><b>Informaci&oacute;n de los productos</b></h4>
				</center>
				<table id="lista_productos" class="table table-bordered table-responsive">
					<thead>
						<tr>
							<th>C&oacute;digo del producto</th>
							<th>Descripci&oacute;n</th>
							<th>Certificado de origen</th>
							<th>Cantidad</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						@foreach($remisiones as $remision)
							<tr>
								<td>
									<input type="text" name="" value="{{ $remision->id_producto }}" disabled="">
								</td>
								<td>
									<input type="text" name="" value="{{ $remision->descripcion_producto }}" disabled="">
								</td>
								<td>
									<input type="text" name="" value="{{ $remision->certificado_de_origen }}" disabled="">
								</td>
								<td>
									<input type="text" name="" value="{{ $remision->cantidad }}" disabled="">
								</td>
								<td>
									<input type="text" name="" value="{{ $remision->total }}" disabled="">
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<!-- VALORES -->
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-7 col-md-7 col-lg-7" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">
						<b>Son:</b>
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="son_remision"disabled="">
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">
						<b>Comentarios</b>
					</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="comentarios_remision"disabled=""></textarea>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-5 col-md-5 col-lg-5" style="padding-top:30px; text-align: center; border: 1px solid black; border-radius: 15px; ">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">
						<b>Subtotal</b>
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="subtotal_remision" disabled=""/>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">
						<b>TOTAL</b>
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="total_remision" disabled=""/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">
	<!-- CONDUCTOR -->
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
				<center>
					<h4><b>Informaci&oacute;n del conductor</b></h4>
				</center>
				<table id="lista_productos" class="table table-bordered table-responsive">
					<thead>
						<tr>
							<th>Placa del vehiculo</th>
							<th>Identificacion del conductor</th>
							<th>Nombre del conductor</th>
							<th>Celular conductor</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							@foreach($remision_unica as $remision_uni)
								<td>
									<input type="text" name="" disabled="" value="{{ $remision_uni->placa_conductor }}">
								</td>
								<td>
									<input type="text" name="" disabled="" value="{{ $remision_uni->identificacion_conductor }}">
								</td>
								<td>
									<input type="text" name="" disabled="" value="{{ $remision_uni->nombres_conductor }} {{ $remision_uni->apellidos_conductor }}">
								</td>
								<td>
									<input type="text" name="" disabled="" value="{{ $remision_uni->celular_1_conductor }} {{ $remision_uni->celular_2_conductor }}">
								</td>
							@endforeach
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>