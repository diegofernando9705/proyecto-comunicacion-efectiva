<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaDetalleCategoriaConferenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_detalle_categoria_conferencia', function (Blueprint $table) {
            $table->increments('id_detalle_cat_con')->autoIncrement();
            $table->integer('id_conferencia_fk');
            $table->integer('id_categoria_fk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_detalle_categoria_conferencia');
    }
}
