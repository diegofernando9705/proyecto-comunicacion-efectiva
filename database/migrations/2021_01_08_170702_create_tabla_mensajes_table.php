<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_mensajes', function (Blueprint $table) {
            $table->increments('id_mensaje')->autoIncrement();
            $table->longText('descripcion_mensaje');
            $table->longText('fecha_mensaje');
            $table->integer('estado_mensaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_mensajes');
    }
}
