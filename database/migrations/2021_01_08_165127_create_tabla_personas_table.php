<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_personas', function (Blueprint $table) {
            $table->increments('identificacion_persona')->autoIncrement();
            $table->longText('nombres_persona');
            $table->longText('apellidos_persona');
            $table->string('email_persona', 100)->unique();
            $table->longText('telefono_persona')->nullable();
            $table->longText('celular_persona')->nullable();
            $table->longText('asociado_persona');
            $table->longText('beneficiario_persona');
            $table->integer('regional_persona');
            $table->longText('rol_persona');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_personas');
    }
}
