<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaAccesosHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_accesos_historial', function (Blueprint $table) {
            $table->increments('id_acceso_historial')->autoIncrement();
            $table->longText('id_persona_fk_ah');
            $table->longText('id_conferencia_fk_ah');
            $table->longText('fecha_acceso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_accesos_historial');
    }
}
