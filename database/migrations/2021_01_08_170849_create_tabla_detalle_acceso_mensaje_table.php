<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaDetalleAccesoMensajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_detalle_acceso_mensaje', function (Blueprint $table) {
            $table->increments('id_detalle_acc_mje')->autoIncrement();
            $table->integer('id_acceso_historial_fk');
            $table->integer('id_mensaje_fk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_detalle_acceso_mensaje');
    }
}
