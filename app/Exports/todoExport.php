<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class todoExport implements FromCollection
{
    public function collection()
    {

    	$resultado = [];

        $reportes = DB::table('accesos')
                    ->select('accesos.*','accesos_historial.*','mensajes.*')
                    ->join('accesos_historial', 'accesos.identificacion','=','accesos_historial.identificacion_fk')
                    ->join('mensajes','accesos_historial.code_acceso','=','mensajes.code_acceso_fk')
                    ->where('accesos.regional', '=', '1')
                    ->get();

       	$resultado[] = $reportes;

        return $resultado;
    }
}

?>