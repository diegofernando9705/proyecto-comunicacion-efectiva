<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ConferenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conferencias = DB::table('tabla_detalle_categoria_conferencia')
                        ->select('tabla_detalle_categoria_conferencia.*', 'tabla_conferencias.*', 'tabla_categoria_conferencia.*')
                        ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                        ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')->paginate(10);

        return view('conferencias.index', compact('conferencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categorias_conferencias = DB::table('tabla_categoria_conferencia')->where('estado_categoria', 1)->get();

        return view('conferencias.crear', compact('categorias_conferencias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $request->validate([
            'categoria' => 'required',
            'descripcion_conferencia' => 'required',
            'estado_conferencia' => 'required'
        ]);

        DB::table('tabla_conferencias')->insert([
            'descripcion_conferencia' => $request->descripcion_conferencia,
            'estado_conferencia' => $request->estado_conferencia
        ]);

         $conferencias = DB::table('tabla_conferencias')->where('estado_conferencia', 1)->orderby('id_conferencia','DESC')->take(1)->get();

         foreach ($conferencias as $conferencia) {
            DB::table('tabla_detalle_categoria_conferencia')->insert([
                'id_conferencia_fk' => $conferencia->id_conferencia,
                'id_categoria_fk' => $request->categoria
            ]);
         };

         return redirect()->route('conferencias.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categorias_conferencias = DB::table('tabla_categoria_conferencia')->where('estado_categoria', 1)->get();

        $conferencias = DB::table('tabla_detalle_categoria_conferencia')
                        ->select('tabla_detalle_categoria_conferencia.*', 'tabla_conferencias.*', 'tabla_categoria_conferencia.*')
                        ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                        ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                        ->where('tabla_conferencias.id_conferencia', $id)->get();
    
        return view('conferencias.edicion', compact('conferencias', 'categorias_conferencias')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'categoria' => 'required',
            'descripcion_conferencia' => 'required',
            'estado_conferencia' => 'required'
        ]);

        DB::table('tabla_conferencias')->where('id_conferencia', $id)->update([
            'descripcion_conferencia' => $request->descripcion_conferencia,
            'estado_conferencia' => $request->estado_conferencia
        ]);

        DB::table('tabla_detalle_categoria_conferencia')->where('id_conferencia_fk', $id)->update([
            'id_categoria_fk' => $request->categoria
        ]);

        
        return redirect()->route('conferencias.index')->with('status_success','Actualizacion exitosa!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        DB::table('tabla_conferencias')->where('id_conferencia', $id)->update([
            'estado_conferencia' => 0
        ]);

        return redirect()->route('conferencias.index')->with('status_success','Borrado exitoso!'); 
    }
}
