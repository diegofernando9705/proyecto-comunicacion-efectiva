<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CategoriasConferenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = DB::table('tabla_categoria_conferencia')->paginate(10);

        return view('categorias.conferencias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias.conferencias.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion_categoria' => 'required',
            'estado_categoria' => 'required'
        ]);

        DB::table('tabla_categoria_conferencia')->insert([
            'descripcion_categoria' => $request->descripcion_categoria,
            'estado_categoria' => $request->estado_categoria
        ]);

        return redirect()->route('categoriasconferencias.index')->with('status_success','Registro exitoso!'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categorias = DB::table('tabla_categoria_conferencia')->where('id_categoria', $id)->get();

        return view('categorias.conferencias.edicion', compact('categorias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = DB::table('tabla_categoria_conferencia')->where('id_categoria', $id)->get();

        return view('categorias.conferencias.edicion', compact('categorias'));
    }

    public function keyup($data){
        
        $categorias = DB::table('tabla_categoria_conferencia')
                    ->where("descripcion_categoria", 'like', $data . "%")->get();

        return view('categorias.conferencias.keyup', compact('categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'descripcion_categoria' => 'required',
            'estado_categoria' => 'required'
        ]);

        $categorias = DB::table('tabla_categoria_conferencia')->where('id_categoria', $id)->update([
            'descripcion_categoria' => $request->descripcion_categoria,
            'estado_categoria' => $request->estado_categoria
        ]);

        return redirect()->route('categoriasconferencias.index')->with('status_success','Actualizacion exitosa!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table('tabla_categoria_conferencia')->where('id_categoria', $id)->update([
            'estado_categoria' => '0',
        ]);

        return redirect()->route('categoriasconferencias.index')
            ->with('status_success','Borrado exitoso!');
    }
}
