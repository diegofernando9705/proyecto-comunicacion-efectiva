<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegionalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regionales = DB::table('tabla_regionales')->paginate(10);

        return view('regionales.index', compact('regionales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('regionales.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion_regional' => 'required',
            'estado_regional' => 'required'
        ]);

        DB::table('tabla_regionales')->insert([
            'descripcion_regional' => $request->descripcion_regional,
            'estado_regional' => $request->estado_regional
        ]);

        return redirect()->route('regionales.index')->with('status_success','Registro exitoso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regionales = DB::table('tabla_regionales')->where('id_regional', $id)->get();

        return view('regionales.edicion', compact('regionales'));
    }

    public function keyup($data){
        
        $regionales = DB::table('tabla_regionales')
                    ->where("descripcion_regional", 'like', $data . "%")->get();

        return view('regionales.keyup', compact('regionales'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'descripcion_regional' => 'required',
            'estado_regional' => 'required'
        ]);

        DB::table('tabla_regionales')->where('id_regional', $id)->update([
            'descripcion_regional' => $request->descripcion_regional,
            'estado_regional' => $request->estado_regional
        ]);

        return redirect()->route('regionales.index')->with('status_success','Registro exitoso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tabla_regionales')->where('id_regional', $id)->update([
            'estado_regional' => '0',
        ]);

        return redirect()->route('regionales.index')
            ->with('status_success','Borrado exitoso!');
    }
}
