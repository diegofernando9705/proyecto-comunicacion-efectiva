<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Exports\todoExport;
use Barryvdh\DomPDF\Facade as PDF;
use Response;

class ReportesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        Gate::authorize('haveaccess', 'reportes.index');

        return view('reportes.index');
    }

    public function completo(){

        $conferencias = DB::table('tabla_conferencias')->where('estado_conferencia', 1)->get();
        $categorias = DB::table('tabla_categoria_conferencia')->where('estado_categoria', 1)->get();
        $regionales = DB::table('tabla_regionales')->where('estado_regional', 1)->get();

        return view('reportes.completo', compact('conferencias', 'categorias', 'regionales'));
    }

    public function formulario($form) {
        //return $form;

        Gate::authorize('haveaccess', 'reportes.index');


        $regionales = DB::table('regionales')->get();

        $resultado = [];


        if ($form == 'todo') {

            $clave_conferencia = [];

            $valores = DB::table('accesos_historial')->select('conferencia')->get();

            foreach ($valores as $value) {
                $clave_conferencia[] = $value->conferencia;
            }

            $lista_simple = array_values(array_unique($clave_conferencia));

            return view('reportes.formularios.todo', compact('regionales', 'lista_simple'));
        } else if ($form == 'accesos') {

            $clave_conferencia = [];
            $valores = DB::table('accesos_historial')->select('conferencia')->get();

            foreach ($valores as $value) {
                $clave_conferencia[] = $value->conferencia;
            }

            $lista_simple = array_values(array_unique($clave_conferencia));

            return view('reportes.formularios.accesos', compact('regionales', 'lista_simple'));
        } else if ($form == 'mensajes') {

            return view('reportes.formularios.mensajes');
        } else {
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generar(Request $request) {

        $reportes = [];

        /* CONFERENCIA */
        if($request->conferencia == ""){
            $registro_conferencia = "0";
        }else{
            foreach ($request->conferencia as $valor) {
                $array_conferencia = $valor;
            }

            if($array_conferencia == '0'){
                $registro_conferencia = "0";
            }
        }

        if(isset($registro_conferencia)){
            $cantidad_conferencia = "0";
        }else{
            $cantidad_conferencia = "1";
        }
        

        /* CATEGORIA */

        if($request->categoria == ""){
            $registro_categoria = "0";
        }else{
            foreach ($request->categoria as $valor) {
                $array_categoria = $valor;
            }

            if($array_categoria == '0'){
                $registro_categoria = "0";
            }
        }

        if(isset($registro_categoria)){
            $cantidad_categoria = "0";
        }else{
            $cantidad_categoria = "1";
        }




        // Todos los campos estan vacios
        if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->paginate(50);

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            }

            return view('reportes.respuestas.vista1', compact('reportes'));

        // Solos los campos de fechas estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            } 

            return view('reportes.respuestas.vista2', compact('reportes'));

        // Campos de fechas y Conferencias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->paginate(50);

                $reportes[] = $resultado;
            }

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solos los campos de fechas y categorias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                }
            }

            return view('reportes.respuestas.vista4', compact('reportes'));

        // Solos los campos de fechas y asociado estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->paginate(50);

            $reportes[] = $resultado;
            

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solos los campos de fechas y regional estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de Conferencia esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->paginate(50);

                $reportes[] = $resultado;
            }

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de Conferencia y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->paginate(50);

                $reportes[] = $resultado;
            }

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de Conferencia y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->paginate(50);

                $reportes[] = $resultado;
            }

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de categoria esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                }
            }

            return view('reportes.respuestas.vista4', compact('reportes'));

        // Solo los campos de categorias y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                }
            }

            return view('reportes.respuestas.vista4', compact('reportes'));

        // Solo los campos de categorias y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                }
            }

            return view('reportes.respuestas.vista4', compact('reportes'));

        // Solo los campos de categorias, asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                }
            }

            return view('reportes.respuestas.vista4', compact('reportes'));

        // Solo el campo de asociado esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->paginate(50);

            $reportes[] = $resultado;
            

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            

            return view('reportes.respuestas.vista3', compact('reportes'));

        // Solo el campo de regional esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            

            return view('reportes.respuestas.vista3', compact('reportes'));

        }else{
                        

        }
    }

    public function exportarPDF(Request $request) {

        $reportes = [];

        /* CONFERENCIA */
        if($request->conferencia == ""){
            $registro_conferencia = "0";
        }else{
            foreach ($request->conferencia as $valor) {
                $array_conferencia = $valor;
            }

            if($array_conferencia == '0'){
                $registro_conferencia = "0";
            }
        }

        if(isset($registro_conferencia)){
            $cantidad_conferencia = "0";
        }else{
            $cantidad_conferencia = "1";
        }
        

        /* CATEGORIA */

        if($request->categoria == ""){
            $registro_categoria = "0";
        }else{
            foreach ($request->categoria as $valor) {
                $array_categoria = $valor;
            }

            if($array_categoria == '0'){
                $registro_categoria = "0";
            }
        }

        if(isset($registro_categoria)){
            $cantidad_categoria = "0";
        }else{
            $cantidad_categoria = "1";
        }

         $array_conferencia = [];
            $resultado_mensajes = [];
            $reportes = [];

        // Todos los campos estan vacios
        if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

           


            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->get();

            $conferencias = DB::table('tabla_accesos_historial')
                        ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            }

            foreach ($conferencias as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                 if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                 }
            }

            $mensajes_totales = sizeof($resultado_mensajes);

           // return $mensajes_totales;

            $pdf = PDF::loadView('reportes.exportado.pdf.vista1', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solos los campos de fechas estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            } 

            $conferencias = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            foreach ($conferencias as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);

            $pdf = PDF::loadView('reportes.exportado.pdf.vista2', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'FINAL.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;


        // Campos de fechas y Conferencias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $conferencias = [];

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                
                $conferencias[] = DB::table('tabla_accesos_historial')
                                    ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                                    ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                                    ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                                    ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                                    ->get();
            }
            

            foreach ($conferencias as $id_conferencia) {
                foreach ($id_conferencia as $codigo) {
                    $array_conferencia[] = $codigo->id_acceso_historial;
                }
                //echo $id_conferencia['id_acceso_historial'];
                //$array_conferencia[] = $id_conferencia->id_acceso_historial;
            }
            

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();


            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);


            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solos los campos de fechas y categorias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];
            $sql_conferencia = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;
                $conferencias[] = $informacion;
                
            }

           foreach ($conferencias as $id_conferencia) {
                foreach ($id_conferencia as $codigo) {

                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $codigo->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $sql_conferencia[] = $resultado;
                }
            }
            

            foreach ($sql_conferencia as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_acceso_historial;
                }
            }

            

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();


            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);


            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solos los campos de fechas y asociado estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->get();

            $reportes[] = $resultado;
            
            $conferencias = DB::table('tabla_accesos_historial')
                ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($resultado as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);    




            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solos los campos de fechas y regional estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $array_conferencia = [];

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->get();

            $reportes[] = $resultado;

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($reportes as $code) {
                foreach ($code as $id_conferencia) {
                    # code...
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
                }
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);    



            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'FINAL.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de Conferencia esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $array_conferencias = [];
            $array_conferencias_sql = [];


            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_conferencias_sql[] = $resultado;
            }

            foreach ($array_conferencias_sql as $codigo) {
                foreach ($codigo as $id_conferencia) {
                    $array_conferencia[] = $id_conferencia->id_conferencia;
                }
            }

            $sql = DB::table('tabla_detalle_acceso_mensaje')->get();

            foreach ($sql as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->id_acceso_historial_fk;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);    



            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'FINAL.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de Conferencia y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $array_resultado = [];
            $array_conferencia = [];

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_resultado[] = $resultado;
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de Conferencia y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_resultado[] = $resultado;
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de categoria esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo los campos de categorias y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista4', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo los campos de categorias y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;$array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista4', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo los campos de categorias, asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista4', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de asociado esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;
          
            

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solo el campo de regional esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);    

             $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        }else{

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /*
     *
     *
     * *
     * *
     * *
     * 
     */

   
    public function exportarCSV(Request $request) {
        $reportes = [];

        /* CONFERENCIA */
        if($request->conferencia == ""){
            $registro_conferencia = "0";
        }else{
            foreach ($request->conferencia as $valor) {
                $array_conferencia = $valor;
            }

            if($array_conferencia == '0'){
                $registro_conferencia = "0";
            }
        }

        if(isset($registro_conferencia)){
            $cantidad_conferencia = "0";
            }else{
            $cantidad_conferencia = "1";
        }
        

        /* CATEGORIA */

        if($request->categoria == ""){
            $registro_categoria = "0";
            }else{
            foreach ($request->categoria as $valor) {
                $array_categoria = $valor;
            }

            if($array_categoria == '0'){
                $registro_categoria = "0";
            }
        }

        if(isset($registro_categoria)){
            $cantidad_categoria = "0";
            }else{
            $cantidad_categoria = "1";
        }

        $array_conferencia = [];
            $resultado_mensajes = [];
            $reportes = [];

        // Todos los campos estan vacios
        if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

           


            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->get();

            $conferencias = DB::table('tabla_accesos_historial')
                        ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            }

            foreach ($conferencias as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                 if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                 }
            }

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $reporte) {
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solos los campos de fechas estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $resultados = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            foreach ($resultados as $resultado) {
                $reportes[] = $resultado;
            } 

            $conferencias = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();

            foreach ($conferencias as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $reporte) {
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Campos de fechas y Conferencias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $conferencias = [];

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                
                $conferencias[] = DB::table('tabla_accesos_historial')
                                    ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                                    ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                                    ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                                    ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                                    ->get();
            }
            

            foreach ($conferencias as $id_conferencia) {
                foreach ($id_conferencia as $codigo) {
                    $array_conferencia[] = $codigo->id_acceso_historial;
                }
                //echo $id_conferencia['id_acceso_historial'];
                //$array_conferencia[] = $id_conferencia->id_acceso_historial;
            }
            

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();


            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);


        // Solos los campos de fechas y categorias estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];
            $sql_conferencia = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;
                $conferencias[] = $informacion;
                
            }

           foreach ($conferencias as $id_conferencia) {
                foreach ($id_conferencia as $codigo) {

                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $codigo->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $sql_conferencia[] = $resultado;
                }
            }
            

            foreach ($sql_conferencia as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_acceso_historial;
                }
            }

            

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->whereBetween('tabla_detalle_acceso_mensaje.created_at', [$request->fecha_inicial, $request->fecha_final])
                        ->get();


            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            $mensajes_totales = sizeof($resultado_mensajes);


            $pdf = PDF::loadView('reportes.exportado.pdf.vista3', compact('reportes', 'mensajes_totales'));
              $path = public_path('pdf/');
            $fileName =  time().'-comunicacion-efectiva.'. 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/'.$fileName);
            $pdf_url = env('APP_URL').$path.$fileName;


            $url = public_path('pdf/').$fileName;
            
            return $url;

        // Solos los campos de fechas y asociado estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->get();

            $reportes[] = $resultado;
            
            $conferencias = DB::table('tabla_accesos_historial')
                ->select('tabla_conferencias.*', 'tabla_accesos_historial.*')
                ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                ->get();

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($resultado as $id_conferencia) {
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }
            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solos los campos de fechas y regional estan con informacion
        }else if($request->fecha_inicial != "" and 
            $request->fecha_final != "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $array_conferencia = [];

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->whereBetween('tabla_accesos_historial.fecha_acceso', [$request->fecha_inicial, $request->fecha_final])
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->get();

            $reportes[] = $resultado;

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($reportes as $code) {
                foreach ($code as $id_conferencia) {
                    # code...
                $array_conferencia[] = $id_conferencia->id_acceso_historial;
                }
            }

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }

            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de Conferencia esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $array_conferencias = [];
            $array_conferencias_sql = [];


            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_conferencias_sql[] = $resultado;
            }

            foreach ($array_conferencias_sql as $codigo) {
                foreach ($codigo as $id_conferencia) {
                    $array_conferencia[] = $id_conferencia->id_conferencia;
                }
            }

            $sql = DB::table('tabla_detalle_acceso_mensaje')->get();

            foreach ($sql as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->id_acceso_historial_fk;
                }
            }

            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de Conferencia y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $array_resultado = [];
            $array_conferencia = [];

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_resultado[] = $resultado;
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de Conferencia y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia >= "1" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            foreach ($request->conferencia as $conferencia) {
                
                $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $conferencia)
                        ->get();

                $reportes[] = $resultado;
                $array_resultado[] = $resultado;
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


           /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de categoria esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo los campos de categorias y asociado estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            
            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo los campos de categorias y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->paginate(50);

                    $reportes[] = $resultado;$array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo los campos de categorias, asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria != "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $info_conferencia_categoria = [];

            foreach ($request->categoria as $categoria) {
                
                $informacion = DB::table('tabla_detalle_categoria_conferencia')
                ->select('tabla_detalle_categoria_conferencia.*', 'tabla_categoria_conferencia.*', 'tabla_conferencias.*')
                ->join('tabla_conferencias', 'tabla_detalle_categoria_conferencia.id_conferencia_fk', '=', 'tabla_conferencias.id_conferencia')
                ->join('tabla_categoria_conferencia', 'tabla_detalle_categoria_conferencia.id_categoria_fk', '=', 'tabla_categoria_conferencia.id_categoria')
                ->where('tabla_categoria_conferencia.id_categoria', $categoria)->get();

                $info_conferencia_categoria[] = $informacion;

            }
            

            foreach ($info_conferencia_categoria as $valor) {
                foreach($valor as $valor_final){
                    $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_accesos_historial.id_conferencia_fk_ah', $valor_final->id_conferencia)
                        ->get();

                    $reportes[] = $resultado;
                    $array_resultado[] = $resultado;
                }
            }

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de asociado esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional == "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;
          
            

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de asociado y regional estan con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado != "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.asociado_persona', $request->asociado)
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        // Solo el campo de regional esta con informacion
        }else if($request->fecha_inicial == "" and 
            $request->fecha_final == "" and  
            $cantidad_conferencia == "0" and 
            $cantidad_categoria == "0"  and 
            $request->asociado == "0" and 
            $request->regional != "0"){

            $resultado = DB::table('tabla_accesos_historial')
                        ->select('tabla_personas.*', 'tabla_conferencias.*', 'tabla_accesos_historial.*')
                        ->join('tabla_personas', 'tabla_accesos_historial.id_persona_fk_ah', '=', 'tabla_personas.identificacion_persona')
                        ->join('tabla_conferencias', 'tabla_accesos_historial.id_conferencia_fk_ah', '=', 'tabla_conferencias.id_conferencia')
                        ->where('tabla_personas.regional_persona', $request->regional)
                        ->paginate(50);

            $reportes[] = $resultado;
            $array_resultado[] = $resultado;

            foreach ($array_resultado as $valor) {
                foreach ($valor as $key) {
                    # code...
                    $array_conferencia[] = $key->id_conferencia_fk_ah;
                }
            }

            $mensajes = DB::table('tabla_detalle_acceso_mensaje')
                        ->select('tabla_detalle_acceso_mensaje.*', 'tabla_mensajes.*')
                        ->join('tabla_mensajes', 'tabla_detalle_acceso_mensaje.id_mensaje_fk', '=', 'tabla_mensajes.id_mensaje')
                        ->get();

            foreach ($mensajes as $mensaje) {
                if (is_array($array_conferencia) && in_array($mensaje->id_acceso_historial_fk, $array_conferencia)) {
                    $resultado_mensajes[] = $mensaje->descripcion_mensaje;
                }
            }


            /* VISTA TRES */

            $mensajes_totales = sizeof($resultado_mensajes);

            $fileName = 'reporte-conferencia-ce.csv';

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


            $columns = array('Cedula', 'Nombre', 'Apellido', 'Conferencia', 'Fecha de acceso');
            $columns_mensaje = array('Total de mensajes');
            $espacio = array('');


            $callback = function() use($reportes, $columns, $mensajes_totales, $columns_mensaje, $espacio) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($reportes as $rep) {
                    foreach ($rep as $reporte) {
                        
                    $row['Cedula']  = $reporte->identificacion_persona;
                    $row['Nombre']    = $reporte->nombres_persona;
                    $row['Apellido']    = $reporte->apellidos_persona;
                    $row['Conferencia']  = $reporte->descripcion_conferencia;
                    $row['Fecha de acceso']  = $reporte->fecha_acceso;

                    fputcsv($file, array($row['Cedula'], $row['Nombre'], $row['Apellido'], $row['Conferencia'], $row['Fecha de acceso']));
                    }
                }

                /* espacio */
                
                $fileEspacio = fopen('php://output', 'w');
                fputcsv($fileEspacio, $espacio);

                fputcsv($fileEspacio, array(''));

                /* fin espacio */

                        
                /* total de mensajes */
                $fileMje = fopen('php://output', 'w');
                fputcsv($fileMje, $columns_mensaje);

                $row['Total de mensajes']  = $mensajes_totales;
                fputcsv($fileMje, array($row['Total de mensajes']));
                /* fin total mensajes */



                fclose($fileMje);
                fclose($fileEspacio);

                fclose($file);

            };

            return response()->stream($callback, 200, $headers);

        }else{
            
        }
        
    }

}
